//Application Window Component Constructor
function ApplicationWindow() {

	var win = Ti.UI.createWindow();

	return win;
}

//make constructor function the public component interface
module.exports = ApplicationWindow;
