//Application Window Component Constructor
function ApplicationWindow() {

	//create component instance
	var win = Ti.UI.createWindow({
		backgroundColor:'#ffffff'
	});

	function clickOnStuff(e) {
		Ti.API.info('clickety click!', e);

		Ti.API.info('title = '+e.source.title+' or text = '+e.source.text+', id = '+e.source.btnId);
		e.source.backgroundColor = '#00f';

		if (e.source.btnId == 4) {
			aLabel.fireEvent('click');
		}
		aView.backgroundColor = '#00f';
	}



	var aButton = Ti.UI.createButton({
		title: "ButtonX",
		backgroundColor: '#fff',
		top: 16,
		height: 44,
		width: 120,
		left: 16,
		btnId: 4
	});
	//XXX namngiven func
	aButton.addEventListener('click', clickOnStuff);

	aButton.addEventListener('longpress', clickOnStuff);


	//XXX anonym func
	// aButton.addEventListener('click', function() {
		//här händer det!
		// Ti.API.info('clickety click!');
		// aLabel.backgroundColor = '#00f';
	// });

	var aLabel = Ti.UI.createLabel({
		text: "Hej!",
		font: fc.fonts[17]['pizza'],
		top: 25,
		left: 16,
		zIndex: 9,
		btnId: 99,
		// touchEnabled: true,
		backgroundColor: "#f00"
	});

	aLabel.addEventListener('click', clickOnStuff);


	var txtArea = Ti.UI.createTextArea({
		value: "TextField",
		left: 16,
		top: 25,
		backgroundColor: '#ff0'
	});



	var aView = Ti.UI.createView({
		backgroundColor: '#f0f',
		top: 20,
		left: 20,
		right: 20,
		bottom: 20,
		btnId: 'aView',
		layout: 'vertical'
	});
	aView.addEventListener('click', clickOnStuff);

	win.add(aView);
	aView.add(aButton);
	aView.add(aLabel);
	aView.add(txtArea);


	var b1 = Ti.UI.createView({
		top: 20,
		left: 0,
		height: '14%',
		width: '33%',
		backgroundColor: fc.colors.red
	});
	// win.add(b1);
	var b2 = Ti.UI.createView({
		top: 20,
		// left: 0,
		height: '14%',
		width: '33%',
		backgroundColor: '#0f0'
	});
	// win.add(b2);
	var b3 = Ti.UI.createView({
		top: 20,
		right: 0,
		height: '14%',
		width: '33%',
		backgroundColor: '#00f'
	});
	// win.add(b3);
	var b4 = Ti.UI.createView({
		top: '14%',
		height: '14%',
		left: 0,
		right: 0,
		backgroundColor: '#f0f'
	});
	// win.add(b4);

	return win;
}

//make constructor function the public component interface
module.exports = ApplicationWindow;
