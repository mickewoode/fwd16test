

var fc = require('fonts_and_colors');


// This is a single context application with multiple windows in a stack
(function() {
	var Window = require('AppWin');
	// var Window = require('ApplicationWindow');
	// var Window = require('LayoutWindow');
	new Window().open();
})();
