//Application Window Component Constructor
function LayoutWindow() {

	//create component instance
	var win = Ti.UI.createWindow({
		backgroundColor: '#fff'
	});

	function devColor(theObject, theColor, theOpacity) {
		if (theOpacity === undefined) {
			theOpacity = 0.5;
		}
		if (theColor === undefined) {
			theColor = 'random';
		}
		if (theColor == 'random') {
			theColor = Math.floor(Math.random() * 16777215).toString(16);
			if (theColor.length < 6) {
				theColor = '0' + theColor;
			}
			theColor = '#' + theColor;
		}
		if (theObject !== null){
			if (theObject !== undefined){
				theObject.backgroundColor = theColor;
				theObject.opacity = theOpacity;
			}
		}
	}

	Ti.API.info('Ti.Platform.osname = '+Ti.Platform.osname);

	var OS_is_Android = Ti.Platform.osname == "android";
	var OS_is_iOS = !OS_is_Android;
	var device_is_iPad = Ti.Platform.osname === 'ipad';
	var is_58Inch = Ti.Platform.displayCaps.platformHeight == 812 || Ti.Platform.displayCaps.platformWidth == 812;
	var device_is_iPhoneX = is_58Inch && OS_is_iOS;


	var mainHolder = Ti.UI.createView({
		top: 0,
		left: 0,
		right: 0,
		bottom: 64,
		layout: 'vertical' //vertical, horizontal, composite
	});
	devColor(mainHolder, 'yellow', 1.0);

	win.add(mainHolder);

	// var topV = 0;
	// if (OS_is_Android) {
		// topV = 0;
	// } else if (device_is_iPhoneX) {
		// topV = 44;
	// } else {
		// topV = 20;
	// }
	var topHolder = Ti.UI.createView({
		top: OS_is_Android ? 0 : device_is_iPhoneX ? 44 : 20,
		left: 0,
		right: 0,
		height: Ti.UI.SIZE,
		layout: 'horizontal'
	});
	devColor(topHolder, 'red', 1.0);
	mainHolder.add(topHolder);

	var profileImgView = Ti.UI.createImageView({
		image: 'noimage.png',
		top: 12,
		left: 12,
		width: 50,
		height: 70
	});
	topHolder.add(profileImgView);

	var topLeftHolder = Ti.UI.createView({
		top: 12,
		left: 12,
		right: 12,
		height: Ti.UI.SIZE,
		layout: 'vertical' //vertical, horizontal, composite
	});
	devColor(topLeftHolder, 'blue', 1.0);
	topHolder.add(topLeftHolder);

	var titleLbl = Ti.UI.createLabel({
		text: 'The title (max 1 row in height)\nRow 2',
		textAlign: 'left',
		color: 'black',
		top: 0,
		left: 0,
		right: 0,
		maxLines: 1
	});
	topLeftHolder.add(titleLbl);

	var subTitleLbl = Ti.UI.createLabel({
		text: 'Subtitle (max 4 rows in height)\nThe label can have 1,2,3 or 4 rows.\nRow 3\nRow 4\nRow 5',
		// text: 'Subtitle (max 4 rows in height)\nRow 2',
		// text: 'Subtitle (max 4 rows in height)',
		textAlign: 'left',
		color: 'black',
		top: 8,
		right: 0,
		left: 0,
		maxLines: 4
	});
	topLeftHolder.add(subTitleLbl);

	var tagHolder = Ti.UI.createView({
		top: 8,
		left: 0,
		right: 0,
		height: Ti.UI.SIZE,
		layout: 'horizontal' //vertical, horizontal, composite
	});
	devColor(tagHolder, 'green', 1.0);
	topHolder.add(tagHolder);

	var tags = ["OneTag", "OrMoreTags", "NoMaximumTags", "AllTagsMustShow", "TheGUI_MustAdapt", "ThisIsAVeryMuchLongerTag"];
	for (var i = 0; i < 6; i++) {
		tagHolder.add(Ti.UI.createLabel({
			text: '#'+tags[i],
			textAlign: 'center',
			color: 'black',
			top: 4,
			left: 8,
			backgroundColor: "orange"
		}));
	}

	var emptySpace = Ti.UI.createView({
		top: 0,
		left: 0,
		right: 0,
		height: 12
	});
	topHolder.add(emptySpace);

	devColor(profileImgView, 'yellow', 1.0);
	devColor(titleLbl, 'orange');
	devColor(subTitleLbl, 'orange');

	var middleHolder = Ti.UI.createView({
		top: 0,
		left: 0,
		right: 0,
		bottom: device_is_iPhoneX ? 34 : 0
	});
	devColor(middleHolder, 'green', 1.0);
	mainHolder.add(middleHolder);

	var searchHolder = Ti.UI.createView({
		top: 4,
		left: 0,
		right: 0,
		height: 55
	});
	devColor(searchHolder);
	middleHolder.add(searchHolder);

	var searchImgView = Ti.UI.createImageView({
		image: 'imageFile.png',
		left: 12,
		width: 40,
		height: 40,
		backgroundColor: 'yellow'
	});
	searchHolder.add(searchImgView);

	var searchBtn = Ti.UI.createButton({
		image: 'imageFile.png',
		title: 'Button',
		right: 12,
		width: 70,
		height: 40,
		borderRadius: 6,
		backgroundColor: 'white'
	});
	searchHolder.add(searchBtn);


	var searchTextField = Ti.UI.createTextField({
		left: (searchImgView.left*2) + searchImgView.width,
		right: (searchBtn.right*2) + searchBtn.width,
		value: '',
		height: searchBtn.height,
		hintText: 'TextField -> Search here...',
		borderRadius: 6,
		backgroundColor: 'white'
	});
	searchHolder.add(searchTextField);


	var bottomLbl = Ti.UI.createLabel({
		text: 'A label on the bottom',
		textAlign: 'center',
		color: 'black',
		bottom: 12,
		height: 23,
		// minimumFontSize: 8,
		backgroundColor: 'orange'
	});
	middleHolder.add(bottomLbl);

	var bottomHolder = Ti.UI.createView({
		left: 0,
		right: 0,
		height: 64,
		bottom: device_is_iPhoneX ? 34 : 0
	});
	win.add(bottomHolder);

	for (var i = 0; i < 4; i++) {
		var tmpView = Ti.UI.createButton({
			title: 'Button '+(i+1),
			top: 0,
			left: (25*i)+'%',
			width: '25%',
			height: '100%',
			color: 'black'
		});
		devColor(tmpView, 'blue', (i+1)/4);
		bottomHolder.add(tmpView);
	}

	return win;
}

//make constructor function the public component interface
module.exports = LayoutWindow;
